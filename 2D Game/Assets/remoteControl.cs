﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class remoteControl : MonoBehaviour {

    public bool controlled;
    public float speed = 100;
    public Rigidbody2D rb;

	// Use this for initialization
	void Start () {
        controlled = false;
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (controlled)
        {
            //rb.isKinematic = true;
            if (Input.GetKey(KeyCode.I))
            {
                rb.velocity = Vector2.up * speed;
                print("up");
            }
            if (Input.GetKey(KeyCode.J))
            {
                rb.velocity = -Vector2.right * speed;
                print("left");
            }
            if (Input.GetKey(KeyCode.K))
            {
                rb.velocity = -Vector2.up * speed;
                print("down");
            }
            if (Input.GetKey(KeyCode.L))
            {
                rb.velocity = Vector2.right * speed;
                print("right");
            }
        }
        else {
            //rb.isKinematic = false;
        }
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!controlled)
        {
            if (collision.gameObject.name == "LivesPlayer")
            {
                controlled = true;
                print("REMOTE CONTROL!");
            }
        }
    }
}
