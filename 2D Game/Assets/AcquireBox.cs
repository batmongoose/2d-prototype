﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcquireBox : MonoBehaviour {

    public GameObject equippedBox;
    public string power;
    private Rigidbody2D rb;
    private PowerCounter counterOnBox;
    public int fireworksForceUpDown = 1000;
    public int fireworksForceLeftRight = 2000;

	// Use this for initialization
	void Start () {
        rb = this.GetComponent<Rigidbody2D>();
        counterOnBox = equippedBox.GetComponent<PowerCounter>();
	}
	
	// Update is called once per frame
	void Update () {
		if(equippedBox) {
            if (equippedBox.name == "FireworksBox") {
                power = "fireworks";
            }
        }
        else {
            power = "";
        }

        if (power == "fireworks") {
            //while (counterOnBox.GiveCounter() > 0) {
            if (Input.GetKeyDown(KeyCode.P) && Input.GetKey(KeyCode.RightArrow)) {
                rb.AddForce(transform.right * fireworksForceLeftRight);
            }
            else if (Input.GetKeyDown(KeyCode.P) && Input.GetKey(KeyCode.LeftArrow)) {
                rb.AddForce(-transform.right * fireworksForceLeftRight);
            }
            else if (Input.GetKeyDown(KeyCode.P) && Input.GetKey(KeyCode.DownArrow)) {
                rb.AddForce(-transform.up * fireworksForceUpDown);
            }
            //counterOnBox.UsePower(1);
            else if (Input.GetKeyDown(KeyCode.P)) {
                rb.AddForce(transform.up * fireworksForceUpDown);
                //counterOnBox.UsePower(1);
            }
            
            
            //}
        }
	}
}
