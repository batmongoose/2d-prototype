﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerCounter : MonoBehaviour {

    public int counter = 3;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UsePower(int Ferocity) {
        if (counter > 0 && counter>Ferocity){
            counter = counter - Ferocity;
        }
    }
    public int GiveCounter() {
        return counter;
    }
}
