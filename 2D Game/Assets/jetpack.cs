﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jetpack : MonoBehaviour {

    public Rigidbody2D rb;
    public float jetpackForce = 75.0f;
    public float jetpackSpeed = 12.0f;
    
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
	}

    // Update is called once per frame
    void FixedUpdate() {
        bool jetpackActive1 = Input.GetButton("Fire1");
        bool jetpackActive2 = Input.GetButton("Fire2");

        if (jetpackActive1) {
            rb.AddForce(new Vector2(0, jetpackForce));
            
        }
        if (jetpackActive2) {
            rb.velocity = new Vector2(rb.velocity.x, jetpackSpeed);
        }
    }
}
