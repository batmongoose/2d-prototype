﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxPullRadius : MonoBehaviour {

    public bool inside;
    public int radiusMode;
    public float travelingSpeed=1;

    void Start() {
        inside = false;
    }

    void FixedUpdate() {
        if (Input.GetKey(KeyCode.E)) {
            if (!inside) { }
            //move normally when not inside the Magnet (insert whatever you use to move the coins)
            else if (inside) { //when inside the Magnet apply magnet's forces
                attract();
            }
        }
        if (Input.GetKey(KeyCode.Q)) {
            if (!inside) { }
            //move normally when not inside the Magnet (insert whatever you use to move the coins)
            else if (inside) { //when inside the Magnet apply magnet's forces
                repel();
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other) { //score bubble is set to isTrigger (when it enters another collider this is run)
        if (other.gameObject.tag == "BoxPullArea") { //if inside the magnet
            inside = true;
        }
        //else inside = false; //omit this line if you want the coins to only be attracted to your object while within the magnet. I found this line to be unnecessary for gameplay. 
        if (other.gameObject.tag == "Player") {
            
        }
    }
    
    private void OnTriggerExit(Collider other) {
        if (other.gameObject.tag == "BoxPullArea") {
            inside = false;
        }
    }

    public void attract() {
        /*
        Vector2 direction = (transform.position - GameObject.Find("LivesPlayer").transform.position).normalized; //find the direction to move
        float travelSpeed = Vector2.Distance(transform.position, GameObject.Find("LivesPlayer").transform.position); //set the speed of the score bubble to the distance between it and the player
        //rigidbody2D.AddForce(-direction * travelSpeed); //apply force (This works but can sometimes cause the coins/objects to orbit your player forever
        GetComponent<Rigidbody2D>().velocity = -direction * travelSpeed; //set velocity instead of apply force to avoid orbiting scenarios
        */
        Vector2 direction = (transform.position - GameObject.Find("LivesPlayer").transform.position).normalized;
        GetComponent<Rigidbody2D>().velocity = -direction * travelingSpeed;
    }

    public void repel() {
        /*
        Vector2 direction = (transform.position - GameObject.Find("LivesPlayer").transform.position).normalized; //find the direction to move
        float travelSpeed = Vector2.Distance(transform.position, GameObject.Find("LivesPlayer").transform.position); //set the speed of the score bubble to the distance between it and the player
        //rigidbody2D.AddForce(-direction * travelSpeed); //apply force (This works but can sometimes cause the coins/objects to orbit your player forever
        GetComponent<Rigidbody2D>().velocity = direction * travelSpeed; //set velocity instead of apply force to avoid orbiting scenarios
        */

        Vector2 direction = (transform.position - GameObject.Find("LivesPlayer").transform.position).normalized;
        GetComponent<Rigidbody2D>().velocity = direction * travelingSpeed;
    }
}
