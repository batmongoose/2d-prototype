﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorHandler : MonoBehaviour {

    public GameObject player;
    public GameObject doorSpawn;
    //public GameObject message;
    public bool atDoor=false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        //Vector3 messagePos = Camera.main.WorldToScreenPoint(gameObject.transform.position + new Vector3(0, 2, 0));
        //message.transform.position = messagePos;

        if (Input.GetKeyDown(KeyCode.E)) {
            print("you pressed E");
            player.transform.position = doorSpawn.transform.position;
        }
        if (atDoor) {
            player.transform.position = doorSpawn.transform.position;
            
            if (Input.GetKeyDown(KeyCode.E)) {
                print("you pressed E");
                player.transform.position = doorSpawn.transform.position;
            }
            
        }
    }

    void CheckIfAtDoor(bool result) {
        print(atDoor);
        atDoor = result;
    }

    void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Player") {
            //message.SetActive(true);
            CheckIfAtDoor(true);
        }
    }
    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.tag == "Player") {
            //message.SetActive(false);
            CheckIfAtDoor(false);
        }
    }
}
