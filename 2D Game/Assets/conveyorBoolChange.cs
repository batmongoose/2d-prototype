﻿using UnityEngine;
using System.Collections;

public class conveyorBoolChange : MonoBehaviour {

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    
    
    private void OnCollisionStay2D(Collision2D collision) {
        Collider2D other = collision.collider;
        if (other.tag == "Player") {
            other.GetComponent<SimplePlayer0>().conveyor = true;
        }
    }
}
