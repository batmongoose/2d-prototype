﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntiMagnet : MonoBehaviour {


    public float forceAmount = 100;
    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }


    private void OnTriggerStay2D(Collider2D collision) {
        if (collision.tag == "Magnetic") {
            collision.GetComponent<Rigidbody2D>().gravityScale = 0;
            collision.GetComponent<Rigidbody2D>().AddForce(-(transform.position - collision.transform.position) * forceAmount, ForceMode2D.Force);
            //collision.GetComponent<Rigidbody2D>().AddForce(Vector2.up*forceAmount,ForceMode2D.Impulse);

            //print(collision.GetComponent<Rigidbody2D>());
            //print(collision.transform);
            //print(collision.transform.position - transform.position);

            //transform.LookAt(collision.transform);
            //rb.AddRelativeForce(, power);
        }
    }
    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.tag == "Magnetic") {
            collision.GetComponent<Rigidbody2D>().gravityScale = 1;
        }
    }
}
