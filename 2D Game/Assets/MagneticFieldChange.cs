﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagneticFieldChange : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Magnetic") {
            collision.tag = "Untagged";

        }
        else
            collision.tag = "Magnetic";
    }
}
